package turych.address.model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Alert;
import turych.address.entity.Person;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.util.ArrayList;
import java.util.Properties;
import java.util.prefs.Preferences;

public class PersonModel {

    private String filePath;

    public PersonModel(){
        Properties properties = new Properties();
        try (InputStream fis = new FileInputStream("config.properties")) {
            properties.load(fis);
            this.filePath = properties.getProperty("filePath");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    /**
     * Returns the person file preference, i.e. the file that was last opened.
     * The preference is read from the OS specific registry. If no such
     * preference can be found, null is returned.
     *
     * @return
     */
    public File getPersonFilePath() {
        if (this.filePath != null) {
            return new File(this.filePath);
        } else {
            return null;
        }
    }

    /**
     * Sets the file path of the currently loaded file. The path is persisted in
     * the OS specific registry.
     *
     * @param file the file or null to remove the path
     */
    public void setPersonFilePath(File file) {

            Properties properties = new Properties();
            FileInputStream fis = null;
            FileOutputStream fos = null;
            try {
                fis = new FileInputStream("config.properties");
                properties.load(fis);
                this.filePath = file == null ? null : file.getPath();
                properties.setProperty("filePath", file == null ? "" : file.getPath());
                fos = new FileOutputStream("config.properties");
                properties.store(fos, "no comment");
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (fis != null) {
                    try {
                        fis.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                if (fos != null) {
                    try {
                        fos.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

    }

    /**
     * Loads person data from the specified file. The current person data will
     * be replaced.
     *
     * @param file
     */
    public ObservableList<Person> loadPersonDataFromFile(File file) throws JAXBException{
        JAXBContext context = JAXBContext
                .newInstance(PersonListWrapper.class);
        Unmarshaller um = context.createUnmarshaller();

        // Reading XML from the file and unmarshalling.
        PersonListWrapper wrapper = (PersonListWrapper) um.unmarshal(file);

        ObservableList<Person> personData = FXCollections.observableArrayList();

        personData.addAll(wrapper.getPersons());

        // Save the file path to the registry.
        setPersonFilePath(file);

        return personData;
    }

    /**
     * Saves the current person data to the specified file.
     *
     * @param file
     */
    public void savePersonDataToFile(File file, ObservableList<Person> personData) throws JAXBException {
        JAXBContext context = JAXBContext
                .newInstance(PersonListWrapper.class);
        Marshaller m = context.createMarshaller();
        m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

        // Wrapping our person data.
        PersonListWrapper wrapper = new PersonListWrapper();
        wrapper.setPersons(personData);

        // Marshalling and saving XML to the file.
        m.marshal(wrapper, file);

        // Save the file path to the registry.
        setPersonFilePath(file);
    }
}